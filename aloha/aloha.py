from numpy.random import normal
from numpy import sqrt, exp, maximum, average

s0 = 100
k = 100
r = 0.05
d = 0.00
vol = 0.3
T = 1
path = 300000
cp = 1

df = exp(-r*T)
ln_rt = normal(((r-d)-vol**2/2)*T, vol*sqrt(T), path)
pv = average(maximum(cp*(s0*exp(ln_rt) - k),0))*df
print(pv)
